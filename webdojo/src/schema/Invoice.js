export default {
		'id':    { value: '', type: 'id'  , label: 'Id', editable: false} ,
		'session_id': { value: '', type: 'text', label: 'Title', editable: false} ,
		'comments': { value: '', type: 'text', label: 'Image', editable: true} ,
		'shopping_address1':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'shopping_address2':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'shopping_city':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'shopping_zip':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'shopping_country':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'billing_address1':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'billing_address2':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'billing_city':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'billing_zip':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'billing_country':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'created_at':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'updated_at':  { value: '', type: 'text', label: 'Text' , editable: true} 
}