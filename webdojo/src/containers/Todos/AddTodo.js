import React from 'react'
import {connect} from 'react-redux'
import { addTodo} from '../../actions'

let AddTodo = ({dispatch}) => {
	let input

	return (
	<div>
		<form
			className="pure-form pure-form-aligned"
			onSubmit = { e => {
				e.preventDefault()
				if(!input.value.trim()){
					return
				}
				dispatch(addTodo(input.value))
				input.value = ''
			}}
		>
			<fieldset>
				<legend> Add a todo </legend>
				<div className="pure-control-group">
					<label>Todo</label>
					<input
							ref={node =>
								input = node
							}
						/>
				</div>
				<div className="pure-controls">
					<button className="pure-button pure-button-primary" type="submit">
						Add Todo
					</button>
				</div>
			</fieldset>
		</form>
	</div>
	)
}
AddTodo = connect()(AddTodo)

export default AddTodo