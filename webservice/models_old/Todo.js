var db = require('./index')
var Sequelize = require('sequelize');

const Todo = db.define('todo', {
  completed: Sequelize.BOOLEAN,
  text: Sequelize.STRING
});

  module.exports = Todo;