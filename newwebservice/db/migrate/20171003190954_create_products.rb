class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :title
      t.string :image
      t.string :text
      t.decimal :price
      t.decimal :offerprice

      t.timestamps
    end
  end
end
