import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
// Don't do this! You’re bringing DevTools into the production bundle.
import DevTools from './containers/Common/DevTools';

import Layout1 from './components/Layouts/Layout1'
import Admin from './components/Layouts/Admin'

import TodoPage from './pages/home/TodoPage'
import ProductPageHome from './pages/home/ProductPage'
import ProductPageAdmin from './pages/admin/ProductPage'
import InvoicePageAdmin from './pages/admin/InvoicePage'
import RedditPage from './pages/home/RedditPage'
import Prices from './pages/home/Prices'
import UserPage from './pages/admin/UserPage'
import RolePage from './pages/admin/RolePage'
import ShoppingCart from './pages/home/ShoppingCart'
import Checkout from './pages/home/Checkout'

import configureStore from './store/configureStore.js'
//import {Switch } from 'react-router'
import {  Switch ,BrowserRouter, Route } from 'react-router-dom'

import './index.css';
import './App.css';


const store = configureStore();

render(

  <Provider store={store}>
	  <div>
  		<BrowserRouter>
		  	<Switch>
		  			<Route path="/admin">
		  				<Admin>
		  					<Switch>
								<Route exact path="/admin" component={UserPage} />
								<Route exact path="/admin/users" component={UserPage} />
								<Route exact path="/admin/products" component={ProductPageAdmin} />	
								<Route exact path="/admin/invoices" component={InvoicePageAdmin} />	
								<Route exact path="/admin/roles" component={RolePage} />	
							</Switch>	
		  				</Admin>
					</Route>
					<Route path="/" >
						<Layout1>
							<Switch>
								<Route exact path="/" component={ProductPageHome}/>
								<Route path="/todo" component={TodoPage} />
								<Route path="/products" component={ProductPageHome} />
								<Route path="/reddit/async" component={RedditPage} />
								<Route path="/prices" component={Prices} />
								<Route path="/user" component={UserPage} />
								<Route path="/cart" component={ShoppingCart} />
								<Route path="/checkout" component={Checkout} />
							</Switch>
						</Layout1>
					</Route>
		  		</Switch>
			</BrowserRouter>
			<DevTools/>
	  	</div>
  	</Provider>,
  document.getElementById('root')
)
