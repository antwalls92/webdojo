# spec/factories/invoice.rb
FactoryGirl.define do
  factory :invoice do
  	session_id {SecureRandom.uuid}
    amount { Faker::Number.number(180) } 
    shipping_address1 { Faker::Lorem.word }
    billing_address1 { Faker::Lorem.word }
  end
end