
import axios from 'axios'
import {syncError} from '../actions'

export const Create =  (model_name) => (data)  =>
{
  return dispatch => {

    dispatch(CreateRequest(data, model_name))
    axios.post('http://localhost:3000/'+model_name+'s', data)
    .then(data => { 
      dispatch(CreateResponse(data.data.id, model_name))
      dispatch(Read(model_name)())
    })
    .catch(error => dispatch(syncError('CREATE_'+model_name.toUpperCase(), error)))
    
  }
}

export const Read = (model_name) => () => {
  return dispatch => {
  	
    dispatch(ReadRequest(model_name))
    return axios.get('http://localhost:3000/'+model_name+"s")
      .then( json => dispatch(ReadRequestResponse(json.data,model_name)) )
      .catch(error => dispatch(syncError('READ_'+model_name.toUpperCase(), error)))
  }
}

//talta la U xD

export const Delete = (model_name) => (id) =>{

    return dispatch => {
      dispatch(DeleteRequest(id, model_name))
      axios.delete('http://localhost:3000/'+model_name+'s'+'/'+id)
      .then(data => {
        dispatch(DeleteResponse(id, model_name))
        dispatch(Read(model_name)())
      })
      .catch(error => dispatch(syncError('DELETE_'+model_name.toUpperCase(), error)))
    }
} 



const DeleteRequest = (id, model_name) => 
{
  return {
    type: 'DELETE_'+model_name.toUpperCase()+'_REQUEST',
    id: id
  }
}

const DeleteResponse = (id, model_name) =>
{
  return {
    type: 'DELETE_'+model_name.toUpperCase()+'_RESPONSE',
    id: id
  }
}


const CreateRequest = (data, model_name) => {
  return {
    type: 'CREATE_'+model_name.toUpperCase()+'REQUEST',
    data: data,
  }
}

const CreateResponse = (id ,model_name) => {
  return {
    type: 'CREATE'+model_name.toUpperCase()+'_RESPONSE',
    id: id
  }
}



const ReadRequest = (model_name) => {
  return {
    type: 'READ_'+model_name.toUpperCase()+'_REQUEST'
  }
}

const ReadRequestResponse = (json, model_name) => {
	return{
		type: 'READ_'+ model_name.toUpperCase() +'_RESPONSE',
		items: json,
		receivedAt: Date.now()
	}
}


