'use strict';
module.exports = function(sequelize, DataTypes) {
  var product = sequelize.define('product', {
    title: DataTypes.STRING,
    image: DataTypes.STRING,
    text: DataTypes.TEXT,
    price: DataTypes.DECIMAL,
    offerprice: DataTypes.DECIMAL
  });

  product.associate = function(models) {
      models.product.belongsToMany(models.shoppingcart, { through: 'shoppingcart_products'})
      models.product.belongsToMany(models.invoice, { through: 'invoice_products'})
  };
  return product;
};