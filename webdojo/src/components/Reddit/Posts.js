import React, {Component} from 'react'
import PropTypes from 'prop-types'

export default class Posts extends Component{
	render(){
		return(
		<ul className="pure-menu-list">
			{this.props.posts.map((post,i) => 
				<li className="pure-menu-item" key={i}> 
					{post.title} 
				</li>)}
		</ul>);
		
	}
}

Posts.PropTypes = {
	posts: PropTypes.array.isRequired
}
