import React from 'react'
import logo from '../../logo.svg';
import '../../App.css';
import CSSModules from 'react-css-modules'
import { base, buttons, grids, forms } from 'pure-css'

let styles = {}
Object.assign(styles, base, buttons, grids,forms)


const App = ({children}) => (
  <div className='App'>
  	<div className='App-header header'>
  		<img src={logo} className='App-logo' alt='logo'/>
  		<h2>WebDojo</h2>
  	</div>
  	<p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
    </p>
    <div className="content">
      {children}
    </div>
  </div>
  
)

export default CSSModules(App, styles)