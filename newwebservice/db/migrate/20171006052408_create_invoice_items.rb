class CreateInvoiceItems < ActiveRecord::Migration[5.1]
  def change
    create_table :invoice_items do |t|
      t.belongs_to :invoice
      t.belongs_to :product
      t.float :amount
      t.timestamps
    end
  end
end
