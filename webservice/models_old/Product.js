var db = require('./index')
var Sequelize = require('sequelize');

const Product = db.define('product', {
  title: Sequelize.STRING,
  image: Sequelize.STRING,
  text: Sequelize.STRING,
  price: Sequelize.DECIMAL,
  offerprice: Sequelize.DECIMAL
});



module.exports = Product;