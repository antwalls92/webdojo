import React from 'react'
import '../../css/pricing.css'
import '../../css/base.css'



//TODO: llevar diferentes elementos de la web a componenes
const App = ({children}) => (
 <div>
        <div className="banner">
            <h1 className="banner-head">
                Simple Pricing.<br/>
                Try before you buy.
            </h1>
        </div>

        <div className="l-content">
          <div className="pricing-tables pure-g">
            <div className="pure-u-1 pure-u-md-1-3  ">
                <div className="pricing-table pricing-table-free">
                    <div className="pricing-table-header">
                        <h2>Personal</h2>

                        <span className="pricing-table-price">
                            $20 <span>per month</span>
                        </span>
                    </div>

                    <ul className="pricing-table-list">
                        <li>Free setup</li>
                        <li>Custom sub-domain</li>
                        <li>Standard customer support</li>
                        <li>1GB file storage</li>
                        <li>1 database</li>
                        <li>Unlimited bandwidth</li>
                    </ul>

                    <button className="button-choose pure-button">Choose</button>
                </div>
            </div>

            <div className="pure-u-1 pure-u-md-1-3">
                <div className="pricing-table pricing-table-biz pricing-table-selected">
                    <div className="pricing-table-header">
                        <h2>Small Business</h2>

                        <span className="pricing-table-price">
                            $50 <span>per month</span>
                        </span>
                    </div>

                    <ul className="pricing-table-list">
                        <li>Free setup</li>
                        <li>Use your own domain</li>
                        <li>Standard customer support</li>
                        <li>10GB file storage</li>
                        <li>5 databases</li>
                        <li>Unlimited bandwidth</li>
                    </ul>

                    <button className="button-choose pure-button">Choose</button>
                </div>
            </div>

            <div className="pure-u-1 pure-u-md-1-3">
                <div className="pricing-table pricing-table-enterprise">
                    <div className="pricing-table-header">
                        <h2>Enterprise</h2>

                        <span className="pricing-table-price">
                            $70 <span>per month</span>
                        </span>
                    </div>

                    <ul className="pricing-table-list">
                        <li>Free setup</li>
                        <li>Use your own domain</li>
                        <li>Premium customer support</li>
                        <li>Unlimited file storage</li>
                        <li>25 databases</li>
                        <li>Unlimited bandwidth</li>
                    </ul>

                    <button className="button-choose pure-button">Choose</button>
                </div>
            </div>
        </div> 

        <div className="information pure-g">
            <div className="pure-u-1 pure-u-md-1-2">
                <div className="l-box">
                    <h3 className="information-head">Get started today</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                    </p>
                </div>
            </div>

            <div className="pure-u-1 pure-u-md-1-2">
                <div className="l-box">
                    <h3 className="information-head">Pay monthly or annually</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.
                    </p>
                </div>
            </div>

            <div className="pure-u-1 pure-u-md-1-2">
                <div className="l-box">
                    <h3 className="information-head">24/7 customer support</h3>
                    <p>
                        Cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>
            </div>

            <div className="pure-u-1 pure-u-md-1-2">
                <div className="l-box">
                    <h3 className="information-head">Cancel your plan anytime</h3>
                    <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
        </div> 
    </div> 

   
 
  {children}
  </div>
)

export default App