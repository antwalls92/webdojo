function addModelToState(state, action)
{
    var model= state.items.slice()
    model.push(action.items)
    return model;
}


function user
(
	state =	{
			    isFetching: false,
			    didInvalidate: false,
			    items: []	
			},
	action
) 
{
	switch (action.type) {
    
    case 'READ_USER_REQUEST':
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case 'READ_USER_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.items,
        lastUpdated: action.receivedAt
      })
    case 'CREATE_USER_REQUEST':
        return Object.assign({}, state, {
          isFetching: true,
          didInvalidate: false
        })
    case 'CREATE_USER_RESPONSE':
        return Object.assign({}, state, {
            isFetching: false,
            didInvalidate: false,
            items: addModelToState(state, action)
        })
    case 'DELETE_USER_REQUEST':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
     case 'DELETE_USER_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
    default:
      return state
  }
}

export default user