import React from 'react'
let AdminPageHeader = ({title}) => (
    <div className="pure-g admin-page-header l-box-small" >
            <div className="pure-u-1-8 admin-page-title">
                {title}
            </div>
            <div className="pure-u-1-4 admin-page-search">
                <div className="pure-form pure-form-aligned"> 
                    
                </div>
            </div>
            
    </div>
)

export default AdminPageHeader;