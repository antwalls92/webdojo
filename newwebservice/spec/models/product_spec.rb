# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  title      :string
#  image      :string
#  text       :string
#  price      :decimal(, )
#  offerprice :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# spec/models/todo_spec.rb
require 'rails_helper'

# Test suite for the Product model
RSpec.describe Product, type: :model do

 	subject { described_class.new(
    price: 100, 
    title: 100.0
    ) }

 	describe "Validations" do 
	  it "Is valid with valid attributes" do 
	  	should validate_presence_of(:price)
	  	should validate_presence_of(:title)
	  end
	  it "Is invalid without a price" do 
	  	subject.price = nil
	  	expect(subject).not_to be_valid
	  end
	  it "Is invalid without a title" do 
  		subject.title = nil
	  	expect(subject).not_to be_valid
	  end
  	end
end
