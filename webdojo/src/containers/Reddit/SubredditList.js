import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import{
	selectSubreddit,
	fetchPostsIfNeeded,
	invalidateSubreddit
}from '../../actions'

import Picker from '../../components/Reddit/Picker'
import Posts from '../../components/Reddit/Posts'

class SubredditList extends Component{
	constructor(props)
	{
		super(props)
		this.handleChange = this.handleChange.bind(this)
		this.handleRefreshClick = this.handleRefreshClick.bind(this)
	}

	componentDidMount(){
		const { dispatch, selectedSubreddit } = this.props
		dispatch(fetchPostsIfNeeded(selectedSubreddit))
	}

	componentDidUpdate(prevProps){
		if(this.props.selectedSubreddit !== prevProps.selectedSubreddit)
		{
			const {dispatch, selectedSubreddit} = this.props
			dispatch(fetchPostsIfNeeded(selectedSubreddit))
		}
	}
	handleChange(nextSubreddit){
		const {dispatch} = this.props
		dispatch(selectSubreddit(nextSubreddit))
		dispatch(fetchPostsIfNeeded(nextSubreddit))
	}

	handleRefreshClick(e){
		e.preventDefault();

		const{ dispatch, selectedSubreddit} = this.props
		dispatch(invalidateSubreddit(selectedSubreddit))
		dispatch(fetchPostsIfNeeded(selectedSubreddit))
	}
	render(){
		const{ selectedSubreddit, posts, isFetching, lastUpdated} = this.props
		return (
			<div>
				<Picker
					value={selectedSubreddit}
					onChange={this.handleChange}
					options={['reactjs','options']}
				/>
				<p>
					{lastUpdated &&
						<span>
							Last Updated at {new Date(lastUpdated).toLocaleTimeString()}.{' '}
						</span>	
					}
					{!isFetching &&
						<a  onClick={this.handleRefreshClick}>
							Refresh
						</a>
					}
				</p>
				{isFetching && posts.length === 0 && <h2>Loading...</h2>}
				{!isFetching && posts.length === 0 && <h2>Empty</h2>}
				{posts.length > 0 && 
					<div style={{opacity: isFetching ? 0.5 : 1}}> 
						<Posts posts={posts}/>
					</div>
				}
				
			</div>
		)
	}
	
}

SubredditList.propTypes = {
	selectedSubreddit: PropTypes.string.isRequired,
	posts: PropTypes.array.isRequired,
	isFetching: PropTypes.bool.isRequired,
	lastUpdated: PropTypes.number,
	dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { selectedSubreddit, postsBySubreddit } = state.subreddit
  
	  	 const {
    isFetching,
    lastUpdated,
    items: posts
  } = postsBySubreddit[selectedSubreddit] || {
    isFetching: true,
    lastUpdated: Date.now(),
    items: []
  }

	  return {
	    selectedSubreddit,
	    posts,
	    isFetching,
	    lastUpdated
	  }
  
  
}

SubredditList = connect(mapStateToProps)(SubredditList)

export default SubredditList;
