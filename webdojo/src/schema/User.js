export default {
		'id':       { value: '', type: 'id'  																	, label: 'Id'		, editable: false},
		'username': { value: '', type: 'text'																	, label: 'Username'	, editable: true},
		'password': { value: '', type: 'text'																	, label: 'Password'	, editable: true},
		'image':  	{ value: '', type: 'text'																	, label: 'Image' 	, editable: true},
		'role_id':   { value: '', type: 'select', model: 'role', lookup:"all", lookup_description_field:"role"	, label: 'User Role', editable: false}
}