import react from 'React'
import PropTypes from 'prop-types'

const Text = ({text, onClick}) =>
{
	return <p onClick={onClick}>
		{text}
	</p>
}

Text.propTypes = {
	text: PropTypes.string.isRequired,
	onClick: PropTypes.string
}

export default Image
