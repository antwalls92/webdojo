import React from 'react'
import {connect} from 'react-redux'
//import PropTypes from 'prop-types'


const clickableButton = ({onClickButton, className,text}) =>{
	return(

			<button className={className} onClick={onClickButton}>  
				{text}
			</button>
		);
	
}

clickableButton.propTypes = {
	
}

var ClickableButton = connect()(clickableButton)
export default ClickableButton