

export const AddProduct =  (id)  =>
{
  return {

    type: "ADD_PRODUCT_TO_SHOPPING_CART",
    id: id
  }
}