import React, {Component} from 'react'
import {connect} from 'react-redux'

import {Create, Read} from '../../actions/api'
import {mapStateToProps} from '../../utils/ShoppingCart'
import NumericInput from '../../components/Common/NumericInput'

class ShoppingCart extends Component {


	componentDidMount()
	{
		const {dispatch} = this.props
		dispatch(Read('product')())
	}
	render(){

		const {dispatch, products} = this.props

		return (


			<div className="shopping-cart-page l-box">
					
					<div className="pure-u-1-2 l-containner">
						<div className="pure-g  l-containner-title" >
							<div className="pure-u-1">
								<h2>Your cart</h2>
							</div>
						</div>
						<div className="pure-g  l-containner-head" >
							<div className="pure-u-1-8">
							</div>
							<div className="pure-u-1-8 l-box">
								Description
							</div>
							<div className="pure-u-1-8 l-box">
								Price
							</div>
							<div className="pure-u-1-4 l-box">
								Units
							</div>
							<div className="pure-u-1-8 l-box">
								Total
							</div>
						</div>
						{ products.map(product => (<div className="shoping-cart-item l-box">
		                                    <div className="pure-g">
		                                        <div className="pure-u-1-8  ">
		                                            <img className="product-image" src={product.image}/>
		                                        </div>
		                                        <div className="pure-u-1-8  l-box">
		                                            <div className="pure-g">
		                                                <div className="pure-u-1 shoping-cart-item-title"> {product.title} </div>
		                                            </div>
		                                            
		                                            <div className="pure-g">
		                                                <div className="pure-u-1"> {product.price}€</div>
		                                            </div>
		                                        </div>
		                                        <div className="pure-u-1-8  l-box">
		                                           <div className="pure-g">
		                                                <div className="pure-u-1 shoping-cart-item-field"> {product.price}€</div>
		                                            </div>

		                                        </div>
		                                        <div className="pure-u-1-4  l-box">
		                                           <div className="pure-g">
		                                                <div className="pure-u-1 shoping-cart-item-field"> 
	                                                		<NumericInput defaultValue={product.units}/>
		                                                </div>
		                                            </div>

		                                        </div>
		                                        <div className="pure-u-1-8  l-box">
		                                           <div className="pure-g">
		                                                <div className="pure-u-1 shoping-cart-item-field"> {product.units * product.price}€</div>
		                                            </div>

		                                        </div>
		                                    </div>
		                                    
		                                </div>

		                            ))
					}
					<div className="shoping-cart-buttons l-box-small">
						<div className="pure-g">
							<div className="pure-u-1-2 align-left"><button className="pure-button "> Vaciar la cesta </button></div>
							<div className="pure-u-1-2 align-right"><button className="pure-button "> Seguir comprando </button></div>
						</div>
						
					</div>
					</div>
					<div className="pure-u-1-3 l-containner">
						<div className="pure-g">
							<div className="pure-u-1">
								<div className="pure-g  l-containner-title" >
									<div className="pure-u-1">
										<h2>Summary</h2>
									</div>
								</div>

								<div className="l-box">
									<div className="pure-g" >
										<div className="pure-u-1 align-left">
											<b>Shipping method</b>
										</div>
										
									</div>

									<div className="pure-g " >
										<div className="pure-u-1 align-left">
											<input className="l-box-small" type="radio"/>
											<span className="l-box-small">Postal Service</span>
										</div>
									</div>
									<div className="pure-g" >
										<div className="pure-u-1 align-left">
											<input className="l-box-small" type="radio"/>
											<span className="l-box-small">UPS</span>
										</div>
									</div>
								</div>
								<div className="l-box">
									<div className="pure-g " >
										<div className="pure-u-1-2 align-left ">
											SubTotal
										</div>
										<div className="pure-u-1-2 align-right" >
											{ products.reduce( (total , product ) => { return total + (product.price * product.units) }, 0)}€
										</div>
									</div>
									<div className="pure-g" >
										<div className="pure-u-1-2 align-left ">
											Taxes
										</div>
										<div className="pure-u-1-2 align-right">
											0€
										</div>
									</div>
									
								</div>

								<div>
									<div className="pure-g l-box" >
										<div className="pure-u-1-2 align-left">
											<b>Total</b>
										</div>
										<div className="pure-u-1-2 align-right">
											{ products.reduce( (total , product ) => { return total + (product.price * product.units) }, 0)}€
										</div>
									</div>
								</div>

								<div className="pure-g shoping-cart-item" >
									<div className="pure-u-1 l-box">
										<a href="/checkout">
												<button className="pure-button pure-button-primary wide-button"> Checkout </button>
										</a>
									</div>
									
								</div>

							</div>
						</div>
				
					</div>
			</div>
					
			)
		
	}
}


const WrappedShoppingCart = connect(mapStateToProps)(ShoppingCart)


export default WrappedShoppingCart