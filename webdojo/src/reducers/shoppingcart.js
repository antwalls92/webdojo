import Cookies from 'universal-cookie';
import uuidv4 from 'uuid/v4'
 
const cookies = new Cookies();
const SESSION_ID = "session_id"
const SHOPPING_CART = "shopping_cart"


function addProductToShoppingCart(state, action)
{
    var items= state.items.slice()
    items.push(action.id)

    var shoppingCartFromSession = getShoppingCart();
    shoppingCartFromSession.push(action.id)
    setShoppingCart(shoppingCartFromSession);

    return items;
}

function getSessionId()
{
    var session = cookies.get(SESSION_ID) || uuidv4()
    cookies.set(SESSION_ID, session)
    return session;
}

function getShoppingCart()
{
    var sessionId = getSessionId()
    var sessionStorage = cookies.get(sessionId) || {}
    sessionStorage[SHOPPING_CART] = sessionStorage[SHOPPING_CART] || []
    cookies.set(sessionId, sessionStorage)

    return sessionStorage[SHOPPING_CART];
}

function setShoppingCart(shoppingcart)
{
    var sessionId = getSessionId()
    var sessionStorage = cookies.get(sessionId) || {}
    sessionStorage[SHOPPING_CART] = shoppingcart
    cookies.set(sessionId, sessionStorage)

    return sessionStorage[SHOPPING_CART];
}

function shoppingcart
(
	state =	{
			    isFetching: false,
			    didInvalidate: false,
                session_id: getSessionId(),
			    items: getShoppingCart()	
          
			},
	action
) 
{
	switch (action.type) {
    
    case 'ADD_PRODUCT_TO_SHOPPING_CART':
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false,
        items: addProductToShoppingCart(state, action),
        session_id: state.sessionId
      })
    default:
      return state
  }
}

export default shoppingcart