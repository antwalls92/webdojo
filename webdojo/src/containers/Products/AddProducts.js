import React from 'react'
import {connect} from 'react-redux'
import {addProduct} from '../../actions'
import Form from '../../components/Common/Form'
import ClickableButton from '../../components/Common/ClickableButton'
import {fetchProductsIfNeeded} from '../../actions'
import ProductModel from '../../schema/Product'

let AddProduct = ({dispatch, children}) =>
{
	
	return <Form 
		inputs={ProductModel} 
		legend="Add Product"  
		onSubmit = { input_refs => e => {
				e.preventDefault()
				//TODO: Hacer una funcion que valide los campos del formulario
				/**if(!input_refs['title'].value.trim() || !input_refs['image'].value.trim() || !input_refs['text'].value.trim() ){
					return
				}*/
				console.log(input_refs)

				dispatch(addProduct({title: input_refs['title'].value, image: input_refs['image'].value, text: input_refs['text'].value}))
				input_refs['title'].value = ''
			}}
		buttons= {
			<div>
				<ClickableButton 
					onClickButton={(e) => { 
					e.preventDefault()
					dispatch(fetchProductsIfNeeded()) }} 
					className="pure-button pure-button-secondary" 
					text="update"/>
				<button className="pure-button pure-button-primary" type="submit">
					Add Product
				</button>
			</div>
		}

	/>


}

AddProduct = connect()(AddProduct)

export default AddProduct