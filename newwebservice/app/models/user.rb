# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  username   :string
#  password   :string
#  image      :string
#  role_id    :integer
#  first_name :string
#  last_name  :string
#  email      :string
#  phone      :string
#  address1   :string
#  address2   :string
#  city       :string
#  zip        :string
#  country    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ApplicationRecord
	#Associations
		has_one :role
	#Validations
		validates_presence_of(:password, :username, :role)

end
