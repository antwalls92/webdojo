import {connect} from 'react-redux'
import React from 'react'
import {Component} from 'react'
import ProductGrid from '../../components/Products/ProductGrid'
import ProductList from '../../components/Products/ProductList'
import List from '../../components/Common/List'
import ProductModel from '../../schema/Product'

class ProductListAsync extends Component{

	render()
	{
		const {products, children} = this.props 
		return (
			<div>
				{	
					products &&
					<div>
						<ProductList products={products}/>
						<List models={products} model_description={ProductModel}/>
					</div>
				}
			</div>
			)
	}
}

const mapStateToProps = state => {
	return {
		products: state.products.products
	}
}


var ProductListAsyncWrapped = connect(mapStateToProps)(ProductListAsync)



export default ProductListAsyncWrapped