# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  title      :string
#  image      :string
#  text       :string
#  price      :decimal(, )
#  offerprice :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Product < ApplicationRecord
	validates_presence_of(:price, :title)
end
