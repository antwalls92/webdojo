import React, {Component} from 'react'
import {connect} from 'react-redux'
import AdminPage from '../../components/Layouts/AdminPage'
import DeleteableList from '../../components/Common/DeleteableList'
import SimpleForm from '../../components/Common/SimpleForm'
import UserModel from '../../schema/User'
import {Create, Read} from '../../actions/api'

class UserPage extends Component {


	componentDidMount()
	{
		const {dispatch} = this.props
		dispatch(Read('user')())
	}
	render(){
		const {dispatch, users} = this.props

		return (
		<AdminPage title="Users">

			<div className="pure-g">
				<div className="pure-u-1 pure-controls top-buttons">
					<button className="pure-button pure-button-primary"> New </button>
				</div>
			</div>

			<div className="l-containner l-box-small">
				<SimpleForm
					model_definition={UserModel} 
					legend="Add a new user" 
					funtionToDispatch= { Create('user')} 
					buttons={<button type="submit" className="pure-button pure-button-primary">Add User</button>} > 
				</SimpleForm>
			</div>

			<DeleteableList models={users} model_name="user" model_description={UserModel} />
			
		</AdminPage>)
	}
}

const mapStateToProps = (state) => 
{
	return {
		users: state.users.items
	}
}

const UserPageWrapped = connect(mapStateToProps)(UserPage)


export default UserPageWrapped