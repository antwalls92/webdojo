export default {
		'id':    { value: '', type: 'id'  , label: 'Id', editable: false} ,
		'session_id': { value: '', type: 'text', label: 'Title', editable: false} ,
		'comments': { value: '', type: 'text', label: 'Image', editable: true} ,
		'shopping_address1':  { value: '', type: 'text', label: 'Delivering Address 1' , editable: true} ,
		'shopping_address2':  { value: '', type: 'text', label: 'Delivering Address 2' , editable: true} ,
		'shopping_city':  { value: '', type: 'text', label: 'Delivering City' , editable: true} ,
		'shopping_zip':  { value: '', type: 'text', label: 'Delivering Zip' , editable: true} ,
		'shopping_country':  { value: '', type: 'text', label: 'Delivering Country' , editable: true} ,
		'billing_address1':  { value: '', type: 'text', label: 'Biilling Address 1' , editable: true} ,
		'billing_address2':  { value: '', type: 'text', label: 'Billing Adress 2' , editable: true} ,
		'billing_city':  { value: '', type: 'text', label: 'Billing City ' , editable: true} ,
		'billing_zip':  { value: '', type: 'text', label: 'Billing Zip' , editable: true} ,
		'billing_country':  { value: '', type: 'text', label: 'Billing Country' , editable: true} ,
		'created_at':  { value: '', type: 'text', label: 'Created At' , editable: true} 
		'updated_at':  { value: '', type: 'text', label: 'Updated At' , editable: true} 
}