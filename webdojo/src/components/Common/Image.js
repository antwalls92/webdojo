import react from 'React'
import PropTypes from 'prop-types'

const Image = ({image, onClick}) =>
{
	return <img src={image} onClick={onClick}/>
}

Image.propTypes = {
	image: PropTypes.string.isRequired,
	onClick: PropTypes.string
}

export default Image
