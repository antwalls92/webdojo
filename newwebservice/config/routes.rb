Rails.application.routes.draw do
  resources :users do
  end

  resources :roles do
  end

  resources :products do
  end

  resources :invoices do
  end
end
