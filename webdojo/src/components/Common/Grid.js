import React from 'react'
import PropTypes from 'prop-types'

const Grid = ({models, ModelComponent, onProductClick}) => (
	<div>
		<div className="pure-g l-box">
			<div className="pure-u-1 l-box-small">
				<h3>Destacados</h3>
			</div>
		</div>
		<div className="pure-g">
			<ul className="grid">
				{models.map(model => (
					<div className="pure-u-1-3 pure-u-md-1-4 pure-u-sm-1">
						<div className="l-box-small">
							<ModelComponent key={model.id} model={model}/>
						</div>
					</div>
				))}
			</ul>
		</div>
	</div>
)

Grid.propTypes = {
	models: PropTypes.array,
	modelComponent: PropTypes.node
}

export default Grid
