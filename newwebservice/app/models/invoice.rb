# == Schema Information
#
# Table name: invoices
#
#  id                :integer          not null, primary key
#  session_id        :uuid
#  comments          :string
#  shipping_address1 :string
#  shipping_address2 :string
#  shipping_city     :string
#  shipping_zip      :string
#  shipping_country  :string
#  billing_address1  :string
#  billing_address2  :string
#  billing_city      :string
#  billing_zip       :string
#  billing_country   :string
#  amount            :float
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Invoice < ApplicationRecord
	#relations
	has_many :invoice_items
	has_many :products, through: :invoice_items 

	#validations
	validates_presence_of(:session_id, :amount, :shipping_address1, :billing_address1)
	validate :amount_should_be_equal_to_sum_of_lines
	
	def amount_should_be_equal_to_sum_of_lines
		if(invoice_items.any? && amount != subtotal)
			errors.add(:amount, "Amount should be the sum of the lines")
		end
	end 

	def subtotal
		invoice_items.sum{|x| x.amount}
	end

end
