var Sequelize = require('sequelize');

const sequelize = new Sequelize(null, null, null, {
  host: 'localhost',
  dialect: 'sqlite',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },

  // SQLite only
  storage: 'db/webdojo.sqlite'
}); 

module.exports = sequelize