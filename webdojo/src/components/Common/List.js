import React from 'react'

let ObjectListItem = ({model, model_description}) => (
	<tr className="object-list-item"> 
		{ 
			Object.keys(model_description).map(field => (
			<td className="object-list-item"> 
				{model[field]}
			</td>))
		} 
		
	</tr>
)
let ObjectListHeader = ({model_description}) => (

	<tr className="object-list-header"> 
		{ 
			Object.keys(model_description).map(field => (
				<th className="object-list-header-item">
					{field}
				</th>))
		} 
	 </tr>
)

export default ({models, model_description}) => (
<div className="pure-g">
	<div className="pure-u-1 l-containner clear-content">
		<table className="object-list">
			<tbody>
				<ObjectListHeader model_description={model_description}/>
				{ 
					models.map( model => (
						<ObjectListItem model={model} model_description={model_description} />
				))}
			</tbody>
		</table>
	</div>
</div>
)