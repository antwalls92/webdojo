function addModelToState(state, action)
{
    var model= state.items.slice()
    model.push(action.items)
    return model;
}


function roles
(
	state =	{
			    isFetching: false,
			    didInvalidate: false,
			    items: []	
			},
	action
) 
{
	switch (action.type) {
    
    case 'READ_ROLE_REQUEST':
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case 'READ_ROLE_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.items,
        lastUpdated: action.receivedAt
      })
    case 'CREATE_ROLE_REQUEST':
        return Object.assign({}, state, {
          isFetching: true,
          didInvalidate: false
        })
    case 'CREATE_ROLE_RESPONSE':
        return Object.assign({}, state, {
            isFetching: false,
            didInvalidate: false,
            items: addModelToState(state, action)
        })
    case 'DELETE_ROLE_REQUEST':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
     case 'DELETE_ROLE_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
    default:
      return state
  }
}

export default roles