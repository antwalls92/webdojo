class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.uuid :session_id
      t.string :comments
      
      t.string :shipping_address1
      t.string :shipping_address2
      t.string :shipping_city
      t.string :shipping_zip
      t.string :shipping_country
      #
      t.string :billing_address1
      t.string :billing_address2
      t.string :billing_city
      t.string :billing_zip
      t.string :billing_country
      

      t.timestamps
    end
  end
end
