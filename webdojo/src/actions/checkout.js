import axios from 'axios'
import {syncError} from '../actions'
import Cookies from 'universal-cookie';
import uuidv4 from 'uuid/v4'
 
const cookies = new Cookies();
const SESSION_ID = "session_id"

//TODO unificar esto con lo de shoppingcart haciendo un helper generico de cookies

function getSessionId()
{
    var session = cookies.get(SESSION_ID) || uuidv4()
    cookies.set(SESSION_ID, session)
    return session;
}

export const UpdateBillingAddress =  (data)  =>
{
  return {

    type: "UPDATE_BILLING_ADDRESS",
    data: data
  }
}

export const UpdateShippingAddress =  (data)  =>
{
  return {

    type: "UPDATE_SHIPPING_ADDRESS",
    data: data
  }
}

export const UpdatePersonalInfo =  (data)  =>
{
  return {

    type: "UPDATE_PERSONAL_INFORMATION",
    data: data
  }
}


const DameImporte = (products, shoppingcart) =>
{
   var importOfX = function(x){
    return parseFloat(products.find(element => x == element.id).price)
  }
  var amount = shoppingcart.reduce((sum,x)=> sum + importOfX(x));

  return amount;
}

const TransformState = (state) => {

  var personal_info = state.checkout.personal_info;
  var shipping_address =  state.checkout.shipping_address;
  var billing_address = state.checkout.billing_address;
  var shoppingcart = state.shoppingcart.items;
  var products = state.products.items;
  
  var amount = DameImporte(products, shoppingcart)

  return {
      products: state.shoppingcart.items,
      session_id: state.shoppingcart.session_id,
      shopping_address1 : shipping_address.address1  , 
      shopping_address2 : shipping_address.address2 , 
      shopping_city : shipping_address.city,
      shopping_zip: shipping_address.zip,
      shopping_country: billing_address.country,
      billing_address1: billing_address.address1,
      billing_address2: billing_address.address2,
      billing_city: billing_address.city,
      billing_zip: billing_address.zip,
      billing_country: billing_address.country,
      amount: amount

    }
}

export const MakeInvoice =  ()  =>
{

  return (dispatch, getState) => {

	var state = getState(); 

  	var data = TransformState(state)
  	dispatch(InvoiceRequest(data))
  	axios.post('http://localhost:3000/invoices',data)
    .then(data => { 
      dispatch(InvoiceResponse(data))
    })
    .catch(error => dispatch(syncError("MAKE_INVOICE", error)))
    
  }
}

 const InvoiceRequest =  (data)  =>
{
  return {
     type: "INVOICE_REQUEST",
     data: data
  }
  
}

 const InvoiceResponse =  (data)  =>
{
  return {
     type: "INVOICE_RESPONSE",
     data: data
  }
  
}