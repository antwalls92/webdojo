import React from 'react'
import {connect} from 'react-redux'
import ClickableButton from './ClickableButton'
import {Delete} from '../../actions/api'
const ObjectListItemBase = ({model, model_description, model_name, dispatch}) => (
	<tr className="object-list-item"> 
		{ 
			Object.keys(model_description).map(field => (
			<td  className="object-list-item"> 
				{model[field]}
			</td>))
			
		} 
		{
			<td>
				<ClickableButton
					onClickButton={ () => {dispatch(Delete(model_name)(model.id))}}
					text="delete"
					className="pure-button pure-button-danger"
				/>
		</td>}
	</tr>
)

const ObjectListItem = connect()(ObjectListItemBase)

const ObjectListHeader = ({model_description}) => (

	<tr className="object-list-header"> 
	{ 
		Object.keys(model_description).map(field => (
			<th key={field} className="object-list-header-item">
				{field}
			</th>))
	} 
	
 	</tr>
)

const DeleteableList = ({models, model_description, model_name}) => (
<div className="pure-g">
	<div className="pure-u-1 l-containner clear-content">
		<table className="object-list">
			<tbody>
				<ObjectListHeader model_description={model_description}/>
				{ 
					models.map( model => (
						<ObjectListItem 
						key={model.id}
						model={model} 
						model_name={model_name} 
						model_description={model_description}
						/>
				))}
			</tbody>
		</table>
	</div>
</div>)

export default DeleteableList