# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  role       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Role < ApplicationRecord
	#associations

	#validations
	validates_presence_of(:role)
end
