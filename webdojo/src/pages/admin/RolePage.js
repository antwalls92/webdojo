import React, {Component} from 'react'
import {connect} from 'react-redux'
import AdminPage from '../../components/Layouts/AdminPage'
import DeleteableList from '../../components/Common/DeleteableList'
import SimpleForm from '../../components/Common/SimpleForm'
import RoleModel from '../../schema/Role'
import {Create, Read} from '../../actions/api'

class RolePage extends Component {


	componentDidMount()
	{
		const {dispatch} = this.props
		dispatch(Read('role')())
	}
	render(){
		const {dispatch, roles} = this.props

		return (
		<AdminPage title="Roles">

			<div className="pure-g">
				<div className="pure-u-1 pure-controls top-buttons">
					<button className="pure-button pure-button-primary"> New </button>
				</div>
			</div>

			<div className="l-containner l-box-small">
				<SimpleForm
					model_definition={RoleModel} 
					legend="Add a new Role" 
					funtionToDispatch= { Create('role')} 
					buttons={<button type="submit" className="pure-button pure-button-primary">Add Role</button>} > 
				</SimpleForm>
			</div>

			<DeleteableList models={roles} model_name="role" model_description={RoleModel} />
			
		</AdminPage>)
	}
}

const mapStateToProps = (state) => 
{
	return {
		roles: state.roles.items
	}
}

const RolePageWrapped = connect(mapStateToProps)(RolePage)


export default RolePageWrapped