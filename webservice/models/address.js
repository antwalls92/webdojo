'use strict';
module.exports = function(sequelize, DataTypes) {
  var address = sequelize.define('address', {
    address1: DataTypes.STRING,
    address2: DataTypes.STRING,
    city: DataTypes.STRING,
    zip:  DataTypes.STRING,
    country: DataTypes.STRING
  });

  return address;
};