# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  username   :string
#  password   :string
#  image      :string
#  role_id    :integer
#  first_name :string
#  last_name  :string
#  email      :string
#  phone      :string
#  address1   :string
#  address2   :string
#  city       :string
#  zip        :string
#  country    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'


RSpec.describe User, type: :model do
  
  subject { described_class.new(
    username: "username", 
    password: "password", 
    role_id: 1
    ) }
  # Association test
  describe "Association" do
  	it "is should have one and only one role" do
  		assc = described_class.reflect_on_association(:role)
        expect(assc.macro).to eq :has_one
  	end
  end
 
  # Validation test
  describe "Validations" do
  	it "is valid with valid attributtes" do 
  		should validate_presence_of(:username) 
      should validate_presence_of(:password)
      should validate_presence_of(:role)
  	end
  	it "is invalid without username" do
  		subject.username = nil
  		expect(subject).not_to be_valid
  	end
  	it "is invalid without password" do 
  		subject.password = nil
  		expect(subject).not_to be_valid
  	end
  	it "is invalid without role" do 
		subject.role = nil
  		expect(subject).not_to be_valid
  	end
  	
  end

end
