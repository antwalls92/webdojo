import React from 'react'
import AddTodo from '../../containers/Todos/AddTodo'
import VisibleTodoList from '../../containers/Todos/VisibleTodoList'
import Footer from '../../components/Todos/Footer'



const TodoPage = () => (
  <div>
  	<div className="page-header">
  		<h1>
  			Todo List
  		</h1>
  		<p>Add todos, and click them in order to mark them as finished. You can also filter them by pending, complete or all</p>
  	</div>
  	<div className="l-content-centered">
  		<div className="pure-g">
  			<div className="pure-u-1">
  				<AddTodo />
    			<VisibleTodoList />
   	 			<Footer />
  			</div>
  		</div>
  	</div>
  </div>
)

export default TodoPage