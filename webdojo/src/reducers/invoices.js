function addModelToState(state, action)
{
    var model= state.items.slice()
    model.push(action.items)
    return model;
}


function invoice
(
	state =	{
			    isFetching: false,
			    didInvalidate: false,
			    items: []	
			},
	action
) 
{
	switch (action.type) {
    
    case 'READ_INVOICE_REQUEST':
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case 'READ_INVOICE_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.items,
        lastUpdated: action.receivedAt
      })
    case 'DELETE_INVOICE_REQUEST':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
     case 'DELETE_INVOICE_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
    default:
      return state
  }
}

export default invoice