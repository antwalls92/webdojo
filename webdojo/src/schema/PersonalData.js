export default {
		'first_name': { value: '', type: 'text', label: 'First Name', editable: true} ,
		'last_name': { value: '', type: 'text', label: 'Last Name', editable: true} ,
		'email':  { value: '', type: 'text', label: 'Email' , editable: true} ,
		'phone': { value: '', type: 'text', label: 'Phone Number' , editable: true} ,
}