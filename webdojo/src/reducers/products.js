function addModelToState(state, action)
{
    var model= state.items.slice()
    model.push(action.items)
    return model;
}


function product
(
	state =	{
			    isFetching: false,
			    didInvalidate: false,
			    items: []	
			},
	action
) 
{
	switch (action.type) {
    
    case 'READ_PRODUCT_REQUEST':
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case 'READ_PRODUCT_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.items,
        lastUpdated: action.receivedAt
      })
    case 'CREATE_PRODUCT_REQUEST':
        return Object.assign({}, state, {
          isFetching: true,
          didInvalidate: false
        })
    case 'CREATE_PRODUCT_RESPONSE':
        return Object.assign({}, state, {
            isFetching: false,
            didInvalidate: false,
            items: addModelToState(state, action)
        })
    case 'DELETE_PRODUCT_REQUEST':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
     case 'DELETE_PRODUCT_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
      })
    default:
      return state
  }
}

export default product