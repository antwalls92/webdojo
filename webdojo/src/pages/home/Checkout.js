import React, {Component} from 'react'
import {connect} from 'react-redux'

import {Create, Read} from '../../actions/api'
import {UpdateBillingAddress, UpdateShippingAddress, UpdatePersonalInfo, MakeInvoice } from '../../actions/checkout'
import {mapStateToProps} from '../../utils/ShoppingCart'
import AdressModel from '../../schema/Adress'
import PersonalDataModel from '../../schema/PersonalData'
import Form from '../../components/Common/SimpleForm'

class Checkout extends Component {


	componentDidMount()
	{
		const {dispatch} = this.props
		dispatch(Read('product')())
	}
	render(){

		const {dispatch, products, session_id, personal_info, shipping_address, billing_address} = this.props
		var personal_info_form_button;
		var shipping_address_form_button;
		var billing_address_form_button;

		return (
				<div className="shopping-cart-page l-box">
						<div className="pure-u-2-3 l-containner">
							<div className="pure-g  l-containner-title">
								<div className="pure-u-1 l-box">
									<h2>Checkout</h2>
								</div>
							</div>
							<div className="pure-g l-box">
								<div className="pure-u-1 align-left l-box">
									<div className="pure-g">
										<div className="pure-u-1">
											<h3>Personal Information</h3>
										</div>
									</div>
									<div className="pure-g">
										<div className="pure-u-1">
											<Form 
												model_definition={PersonalDataModel}
												funtionToDispatch = { UpdatePersonalInfo} 
												buttons={ <button type="submit" ref={ node => personal_info_form_button = node} className="hide"> Test</button>}
												horizontal={true}
											/>
										
										</div>
									</div>
								</div>		
							</div>
							<div className="pure-g l-box">
								<div className="pure-u-1-2 align-left l-box">
									<div className="pure-g">
										<div className="pure-u-1">
											<h3>Billing Address</h3>
										</div>
									</div>
									<div className="pure-g l">
										<div className="pure-u-1">
											<Form 
												model_definition={AdressModel}
												funtionToDispatch = { UpdateBillingAddress} 
												buttons={ <button type="submit" ref={ node => billing_address_form_button = node} className="hide"> Test</button>}
											/>
										</div>
									</div>
								</div>
								<div className="pure-u-1-2 align-left l-box">
									<div className="pure-g">
										<div className="pure-u-1">
											<h3>Shipping Address</h3>
										</div>
									</div>
									<div className="pure-g">
										<div className="pure-u-1">
											<Form 
												model_definition={AdressModel}
												funtionToDispatch = {UpdateShippingAddress} 
												buttons={ <button type="submit" ref={ node => shipping_address_form_button = node} className="hide"> Test</button>}
											/>
										</div>
									</div>
								</div>
							</div>

								<div className="pure-g l-box">
									<div className="pure-u-1-3 l-box"></div>
									<div className="pure-u-1-3 l-box">
										<button 
										onClick={ () => {
												personal_info_form_button.click();
												shipping_address_form_button.click();
												billing_address_form_button.click();
											
												dispatch(MakeInvoice());
										}}
										className="pure-button pure-button-primary wide-button "> Proceed</button>
									</div>
									<div className="pure-u-1-3 l-box"></div>
								</div>

						</div>
				</div>	
			)
		
	}
}
function wrappedMapStateToProps(state)
{
	return Object.assign( mapStateToProps(state) , 
		{ personal_info : state.checkout.personal_info } , 
		{ shipping_address : state.checkout.shipping_address } , 
		{ billing_address : state.checkout.billing_address }  )
}


const WrappedCheckout = connect(wrappedMapStateToProps)(Checkout)


export default WrappedCheckout