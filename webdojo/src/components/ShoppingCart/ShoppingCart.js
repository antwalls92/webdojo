import React,{Component} from 'react' 
import {connect} from 'react-redux'
import CartIcon from 'react-icons/lib/md/shopping-cart'

import {mapStateToProps} from '../../utils/ShoppingCart'

class ShoppingCart extends Component{
	
	constructor(props)
	{
		super(props)
	}
	render()
	{
		const { products } = this.props
		return(
                
			 <div className="shoping-cart">
                    <div className="pure-menu-item"><CartIcon/></div>
                    <div className="shoping-cart-list">
                        {products.map(product => (

                                <div className="shoping-cart-item l-box-small">
                                    <div className="pure-g">
                                        <div className="pure-u-1-4 l-box-small">
                                            <img className="product-image" src={product.image}/>
                                        </div>
                                        <div className="pure-u-3-4 l-box">
                                            <div className="pure-g">
                                                <div className="pure-u-1 shoping-cart-item-title"> {product.title} </div>
                                            </div>
                                            <div className="pure-g">
                                                <div className="pure-u-1 shoping-cart-item-quantity"> Cantidad: {product.units}</div>
                                            </div>
                                            <div className="pure-g">
                                                <div className="pure-u-1 shoping-cart-item-price"> {product.price}€</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            ))}
                		
                		<div className="shoping-cart-buttons l-box-small">
                			<a href="/cart">
                                <button className="pure-button pure-button-primary wide-button"> Ir a la cesta </button>
                		    </a>
                        </div>
                		
                    </div>
          	 </div>

		)
	}
}

const WrappedShoppingCart = connect(mapStateToProps)(ShoppingCart)

export default WrappedShoppingCart