import React from 'react'
import logo from '../../logo.svg';

import '../../css/base.css'

import CSSModules from 'react-css-modules'
import { base, buttons, grids, forms, layouts } from 'pure-css'

let styles = {}
Object.assign(styles, base,forms, buttons, grids, layouts)

//TODO: llevar diferentes elementos de la web a componenes
const App = ({children}) => (
 <div className="pure-g">
    <div className="pure-u-1">
      <div className="pure-g">
        <div className="pure-u-1">
          <div className="menu-header">
            <div className="pure-menu pure-menu-horizontal"> 

              <a className="pure-menu-heading">Antwalls's Web Dojo</a>
              <div className="pure-menu-list pure-u-1-2">
                    <div className="pure-menu-item"><a href="/products" className="pure-menu-link">Products</a></div>
                    <div className="pure-menu-item"><a href="/prices" className="pure-menu-link">Prices</a></div>
                    <div className="pure-menu-item"><a href="/admin" className="pure-menu-link">Admin</a></div>
              </div>

              <div className="pure-menu-list pure-u-1-2 align-right">
                  <div className="pure-menu-item"><a href="/register" className="pure-menu-link">Register</a></div>
                  <div className="pure-menu-item"><a href="/login" className="pure-menu-link">Login</a></div>
              </div>
              
            </div>

            
          </div>
        </div>
      </div>
      
      {/*<div className="slider">
        <div className="pure-g">
            <div className="pure-u-1 pure-u-md-1-1 pure-u-lg-1-1">
              <img src={logo} className='App-logo' alt='logo'/>
              <h2>WebDojo</h2>
            </div>
        </div>
      </div>*/}
      <div className="pure-g">
        <div className="pure-u-1 pure-u-md-1-1 pure-u-lg-1-1">
          <div className="content">
            <div className="main">
              {children}
            </div>
          </div>
         </div>
      </div>
      <div className="pure-g">
        <div className="pure-u-1">
            <div className="footer l-box">
              <p>
                <a>Try now</a> for 30 days. No credit card required. Header image courtesy of <a href='http://unsplash.com/'>Unsplash</a>.
              </p>
            </div>
        </div>
      </div>
      
  </div>
 </div>
 
)

export default CSSModules(App, styles)