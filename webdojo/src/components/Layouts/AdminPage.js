import React from 'react'
import PageHeader from '../../components/Layouts/AdminPageHeader'

let AdminPage = ({title, children}) => (
	 <div id="main" className="pure-u-1">
         <PageHeader title={title}/>
         {children}
     </div>
)

export default AdminPage