import React from 'react'
import Slider from 'react-slick'
import CSSModules from 'react-css-modules'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

class SimpleSlider extends React.Component {
  render() {
   
    var settings = {
      dots: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 5000,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div className="pure-g">
        <div className="pure-u-1">
          <Slider {...settings}>
              <div><img src="http://tuspuzzles.es/puzzles/original/atardecer-568470fbef6d4.jpg"/></div>
              <div><img src="http://www.paisajesbonitos.org/wp-content/uploads/2015/11/paisajes-bonitos-de-otoño-lago.jpg"/></div>
              <div><img src="http://static.vix.com/es/sites/default/files/styles/large/public/imj/nuestrorumbo/L/Los-5-mejores-paisajes-del-mundo-3.jpg?itok=NbbkmSen"/></div>
          </Slider>
        </div>
      </div>
    )
  }
}

export default SimpleSlider