import React from 'react'
import PropTypes from 'prop-types'

class NumericButton extends React.Component{

	constructor(props)
	{
		super(props)
		const {defaultValue} = this.props
		this.state = {value : defaultValue }
	}
	render(){

		const {defaultValue} = this.props

		return(

				<div className="numeric-button pure-g">
					<button onClick={e => {this.state.value = this.state.value - 1 }} className="numeric-button-less pure-u-1-3">-</button>
					<div className="numeric-button-value pure-u-1-3">{this.state.value}</div>
					<button onClick={e => {this.state.value = this.state.value + 1 }} className="numeric-button-more pure-u-1-3">+</button>
				</div>
			)
	}
} 

export default NumericButton