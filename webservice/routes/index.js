
module.exports = function(router){

var db = require('../models/index.js');

var byId = (id) => {return { where: {id: id} } } 
var include = (Models) => {return {include: Models} }
var join = (firstCriteria) => (secondCriteria) => Object.assign({},firstCriteria, secondCriteria)

router.get('/api/products', function(req, res, next) {
  db.product.findAll().then(products => {
    res.json(products);
  });
});

router.get('/api/product/:id', function(req, res, next) {
  db.product.find(byId(req.params.id)).then(product => {
    res.json(product);
  });
});


router.post('/api/product', function(req, res, next) {
  db.product.create(req.body).then(product => {
    res.json(product);
  });
});

router.put('/api/product', function(req, res, next) {
  db.product.update(req.body).then(product => {
    res.json(product);
  });
});

router.delete('/api/product', function(req, res, next) {
  db.product.destroy(byId(req.body.id)).then(product => {
    res.json(product);
  });
});


//----------Todos-------------------------------
router.get('/api/todos', function(req, res, next) {
  db.todo.findAll().then(todos => {
    res.json(todo);
  });
});

router.get('/api/todos/:id', function(req, res, next) {
  db.todo.find(byId(req.params.id)).then(todos => {
    res.json(todo);
  });
});


router.post('/api/todo', function(req, res, next) {
  db.todo.create(req.body).then(todo => {
    res.json(todo);
  });
});

router.put('/api/todo', function(req, res, next) {
  db.todo.update(req.body).then(todo => {
    res.json(todo);
  });
});

router.delete('/api/todo', function(req, res, next) {
  db.todo.destroy(byId(req.body.id)).then(todo => {
    res.json(todo);
  });
});




//----------Users-------------------------------
router.get('/api/users', function(req, res, next) {
  db.user.findAll(include([db.role])).then(users => {
    res.json(users);
  });
});

router.get('/api/user/:id', function(req, res, next) {
  var byIdAndIncludeRole = join( byId(req.params.id ))( include([db.role]) )
  db.user.find(byIdAndIncludeRole).then(users => {
    res.json(users);
  });
});


router.post('/api/user', function(req, res, next) {
  db.user.create(req.body).then(user => {
    res.json(user);
  });
});

router.put('/api/user', function(req, res, next) {
  db.user.update(req.body).then(user => {
    res.json(user);
  });
});

router.delete('/api/user', function(req, res, next) {
  db.user.destroy(byId(req.body.id)).then(user => {
    res.json(user);
  });
});



//----------Roles-------------------------------
router.get('/api/roles', function(req, res, next) {
  db.role.findAll().then(roles => {
    res.json(roles);
  });
});

router.get('/api/role/:id', function(req, res, next) {
  db.role.find(byId(req.params.id)).then(roles => {
    res.json(roles);
  });
});


router.post('/api/role', function(req, res, next) {
  db.role.create(req.body).then(role => {
    res.json(role);
  });
});

router.put('/api/role', function(req, res, next) {
  db.role.update(req.body).then(role => {
    res.json(role);
  });
});

router.delete('/api/role', function(req, res, next) {
  db.role.destroy(byId(req.body.id)).then(role => {
    res.json(role);
  });
});


//invoices
router.get('/api/invoices', function(req, res, next) {
  db.invoice.findAll().then(invoice => {
    res.json(invoice);
  });
});


function getMethods(obj) {
  var result = [];
  for (var id in obj) {
    try {
      if (typeof(obj[id]) == "function") {
        result.push(id + ": " + obj[id].toString());
      }
    } catch (err) {
      result.push(id + ": inaccessible");
    }
  }
  return result;
}

router.post('/api/invoice', function(req, res, next) {
  var data = req.body
  console.log(getMethods(db.invoice));
  var invoice = db.invoice.build({
    session_id: data.session_id
  })
  invoice.setShippingAddress(data.shipping_address)
  invoice.setBillingAddress(data.billing_address)
  data.products.map( product => {invoice.addProduct(products.id) })

  invoice.save()
  .then(role => {
    res.json(role);
  });
  
});

}
