import React from 'react'
import PropTypes from 'prop-types'

const Product = ({model, children}) => (
	
		<div className='product-view'>
			
			<img src={model.image} alt="" className='product-view-image' />
			<div className='product-view-title'>
				{model.title}
			</div>
			
			<div className="product-view-prices">
				<span className="product-view-prices-price" > 	  {model.price}€      </span>
				<span className="product-view-prices-offerprice"> {model.offerprice}€ </span>	
			</div>
			{children}
		</div>
		
)

Product.propTypes = {
	model: PropTypes.object
}

export default Product