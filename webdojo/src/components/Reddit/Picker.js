import React, {Component} from 'react'
import PropTypes from 'prop-types'

export default class Picker extends Component {
	render(){
		const{ value, onChange, options} = this.props

		return (
			<span className="pure-form">
				<h1>Reddit title : {value}</h1>
				<select className="pure-input-1" onChange={e => onChange(e.target.value)} value={value}>
					{options.map(option => (
						<option value={option} key={option}>
							{option}
						</option>
					))}
				</select>
			</span>
		)
	}
}

Picker.propTypes = {
	options: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired 
}
