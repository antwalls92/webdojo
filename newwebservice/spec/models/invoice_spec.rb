# == Schema Information
#
# Table name: invoices
#
#  id                :integer          not null, primary key
#  session_id        :uuid
#  comments          :string
#  shipping_address1 :string
#  shipping_address2 :string
#  shipping_city     :string
#  shipping_zip      :string
#  shipping_country  :string
#  billing_address1  :string
#  billing_address2  :string
#  billing_city      :string
#  billing_zip       :string
#  billing_country   :string
#  amount            :float
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

# spec/models/invoice_spec.rb
require 'rails_helper'


RSpec.describe Invoice, type: :model do
  subject { described_class.new(session_id: SecureRandom.uuid, 
    amount: 180.0, 
    id: 1,
    shipping_address1: "Calle ortega nieto", 
    billing_address1: "Avda de granada 18") 
  }
  # Association test
  describe "Associations" do 

    it "it should have many items" do 
        assc = described_class.reflect_on_association(:invoice_items)
        expect(assc.macro).to eq :has_many
    end
    
  end
   
 
  # Validation test
  describe "Validations" do 
    it "is valid with valid attributes" do
      should validate_presence_of(:session_id)
      should validate_presence_of(:amount) 
      should validate_presence_of(:shipping_address1)
      should validate_presence_of( :billing_address1)
    end
   

    it "is invalid without amount" do 
        subject.amount = nil
        expect(subject).to_not be_valid
    end
    it "is invalid without session_id" do
        subject.session_id = nil
        expect(subject).to_not be_valid
    end
    it "is invalid without billing_address" do 
        subject.billing_address1 = nil
        expect(subject).to_not be_valid
    end
    it "is invalid without shipping_address" do
        subject.shipping_address1 = nil
        expect(subject).to_not be_valid
    end
  end
  
end
