import React from 'react'
import HomeIcon from 'react-icons/lib/fa/home'
import UserIcon from 'react-icons/lib/fa/user'
import DashboardIcon from 'react-icons/lib/md/dashboard'
import PageIcon from 'react-icons/lib/fa/file-o'
import PublicationIcon from 'react-icons/lib/md/public'
import MoneyIcon from 'react-icons/lib/fa/money'
import AdminUserInfo from './AdminUserInfo'

let MenuItem = ({link, icon, title}) => (
    <li className="pure-menu-item"><a href={link} className="pure-menu-link"> {icon}  <span> {title} </span>  </a> </li>
)

let AdminMenu = () => (
    <div id="nav" className="pure-u">
            <a href="#" className="nav-menu-button">Menu</a>
            <div className="pure-g">
                <div className="pure-u-1 menu-title">Administration</div>
            </div>
            <div className="nav-inner">
                <div className="pure-menu">
                    <AdminUserInfo/>
                    <ul className="pure-menu-list">
                        <li className="pure-menu-heading">Main</li>
                        <MenuItem link="/" icon = {<HomeIcon className="menu-icon"/>} title="Home"/>
                        <MenuItem link="/admin/users" icon = {<UserIcon className="menu-icon"/>} title="Users"/>
                        <MenuItem link="/admin/pages" icon = {<PageIcon className="menu-icon"/>} title="Pages"/>    
                        <MenuItem link="/admin/products" icon = {<DashboardIcon className="menu-icon"/>} title="Products"/>
                        <MenuItem link="/admin/publications" icon = {<PublicationIcon className="menu-icon"/>} title="Publications"/>
                        <MenuItem link="/admin/invoices" icon = {<MoneyIcon className="menu-icon"/>} title="Invoices"/>
                        
                        <li className="pure-menu-heading">Labels</li>
                        <li className="pure-menu-item"><a href="#" className="pure-menu-link"><span className="email-label-personal"></span>Personal</a></li>
                        <li className="pure-menu-item"><a href="#" className="pure-menu-link"><span className="email-label-work"></span>Work</a></li>
                        <li className="pure-menu-item"><a href="#" className="pure-menu-link"><span className="email-label-travel"></span>Travel</a></li>

                    </ul>
                </div>
            </div>
          </div>
)

export default AdminMenu