'use strict';
module.exports = function(sequelize, DataTypes) {
  var shoppingcart = sequelize.define('shoppingcart', {
    sessionId: DataTypes.UUID
  });
   shoppingcart.associate = function(models, sequelize) {
       models.shoppingcart.belongsToMany(models.product, { through: 'shoppingcart_products'})
  };
  return shoppingcart;
};