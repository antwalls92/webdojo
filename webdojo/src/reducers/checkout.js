import Cookies from 'universal-cookie';
import uuidv4 from 'uuid/v4'
 
const cookies = new Cookies();
const SESSION_ID = "session_id"

//TODO unificar esto con lo de shoppingcart haciendo un helper generico de cookies

function getSessionId()
{
    var session = cookies.get(SESSION_ID) || uuidv4()
    cookies.set(SESSION_ID, session)
    return session;
}

function checkout
(
	state =	{
			    isFetching: false,
			    didInvalidate: false,
          sessionId: getSessionId(),
          billing_address: {},
          shipping_address: {},
          personal_info: {},
          last_invoice:{}	
          
			},
	action
) 
{
	switch (action.type) {
    
    case 'UPDATE_BILLING_ADDRESS':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        billing_address: action.data,
        sessionId: state.sessionId
      })
    case 'UPDATE_SHIPPING_ADDRESS':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        shipping_address: action.data,
        sessionId: state.sessionId
      })
      case 'UPDATE_PERSONAL_INFORMATION':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        personal_info: action.data,
        sessionId: state.sessionId
      })
      case 'INVOICE_RESPONSE':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        last_invoice: action.data,
        sessionId: state.sessionId
      })

    default:
      return state
  }
}

export default checkout