import React from 'react'

var PageWithHeaderAndContentCentered = ({children, title, subtitle}) => (
  
	<div>
		<div className="page-header">
			<h1 className="information-head">{title}</h1>
		<p> {subtitle} </p>
		</div>
		
		<div className="l-content-centered">
			<div className="pure-g">
				<div className="pure-u-1">
					{children}	
				</div>
			</div>
		</div>
	</div>

);

export default PageWithHeaderAndContentCentered;