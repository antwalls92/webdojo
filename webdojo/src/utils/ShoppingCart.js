var groupBy = function(xs, key) {
  return xs.reduce(function(rv, x) {
    if(key)
    {
        (rv[x[key]] = rv[x[key]] || []).push(x);
    }else
    {
        (rv[x] = rv[x] || []).push(x);
    }
    
    return rv;
  }, {});
};


 export function mapStateToProps(state)
{

    var shoppingCartProductIds = state.shoppingcart.items;
    var products = state.products.items
    if(shoppingCartProductIds.length > 0)
    {
        var productsUnits = groupBy(shoppingCartProductIds);
 
        var shoppingCartProducts = products.filter( product => { 
            return shoppingCartProductIds.includes(product.id)
        });

        products = products.map( product => {
            return Object.assign(product, { units:  productsUnits[product.id] ? productsUnits[product.id].length : 0 })
        })
        return {
            products: shoppingCartProducts,
            session_id: state.shoppingcart.session_id 
        }
    }
    //hacer esto mejor
    return {
            products: [] ,
            session_id: state.shoppingcart.session_id  
    }
    
}