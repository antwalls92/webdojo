# == Schema Information
#
# Table name: invoice_items
#
#  id         :integer          not null, primary key
#  invoice_id :integer
#  product_id :integer
#  amount     :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe InvoiceItem, type: :model do
   subject { described_class.new(invoice_id: 1, 
    product: Product.new(price: 50, title: "title", id: 5), 
    amount: 50.0,
    id: 1)}

   describe "Associations" do

	  it "it should have a product" do 
	  	assc = described_class.reflect_on_association(:product)
	    expect(assc.macro).to eq :belongs_to
  	end

  	it "it should have a invoice" do 
		assc = described_class.reflect_on_association(:invoice)
	    expect(assc.macro).to eq :belongs_to
  	end

   end

  describe "Validations" do
  	it "is valid with valid attributtes" do 
  	  should validate_presence_of(:invoice)
      should validate_presence_of(:product) 
      should validate_presence_of(:amount)
  	end
    

  	it "is not valid without an amount" do 
  		subject.amount = nil
  		expect(subject).not_to be_valid 
  	end

  	
  end
end
