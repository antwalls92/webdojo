'use strict';
module.exports = function(sequelize, DataTypes) {
  var invoice = sequelize.define('invoice', {
    session_id: DataTypes.STRING,
    comments: DataTypes.STRING
  });

   invoice.associate = function(models) {
      models.invoice.belongsToMany(models.product, { through: 'invoice_products'})
      models.invoice.belongsTo(models.address, {as:'ShippingAddress' , foreignKey: 'shipping_address_id'})
      models.invoice.belongsTo(models.address, {as:'BillingAddress'  , foreignKey: 'billing_address_id'})
      models.invoice.belongsTo(models.personal_info)
  };
  return invoice;
};