'use strict';
module.exports = function(sequelize, DataTypes) {
  var user = sequelize.define('user', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    image: DataTypes.STRING
  });

  user.associate = function(models) {
      user.belongsTo(models.role)
  };
 return user;
};