import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Form from './Form'

const SimpleForm = ({model_definition, legend, funtionToDispatch, buttons, dispatch, useLabels, horizontal}) =>
{

	return <Form 
		model_definition={model_definition} 
		legend={legend}  
		onSubmit = { input_refs => e => {
				e.preventDefault()
				//TODO: Hacer una funcion que valide los campos del formulario
				/**if(!input_refs['title'].value.trim() || !input_refs['image'].value.trim() || !input_refs['text'].value.trim() ){
					return
				}*/
				
				
				var data = {}
				Object.keys(input_refs).map( input => data[input] = input_refs[input].value )

				dispatch(funtionToDispatch(data))
				
			}}
		buttons = {buttons}
		horizontal = {horizontal}
		useLabels= {useLabels}
		/>
}

SimpleForm.propTypes = {
	model_definition: PropTypes.object,
	funtionToDispatch: PropTypes.funct,
	legend: PropTypes.string,
	buttons: PropTypes.node
}

const WrappedSimpleForm = connect()(SimpleForm)

export default WrappedSimpleForm
