import React,{Component} from 'react'
import logo from '../../logo.svg';

import '../../css/base.css'
import CSSModules from 'react-css-modules'
import { base, buttons, grids, forms, layouts } from 'pure-css'
import AdminMenu from './AdminMenu'

let styles = {}
Object.assign(styles, base,forms, buttons , grids, layouts)



//TODO: llevar diferentes elementos de la web a componenes
let App = ({children}) => 
(
    <div className="admin">
        <div id="layout" className="content pure-g">
            <AdminMenu/>
            {children}
      </div>
    </div>
)
 

export default CSSModules(App, styles)