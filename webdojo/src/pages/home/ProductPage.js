import React, {Component} from 'react'
import {connect} from 'react-redux'
import Slider from '../../components/Common/Slider'
import Grid from '../../components/Common/Grid'
import Product from '../../components/Products/Product'
import ProductWithShoppingCart from '../../components/Products/ProductWithShoppingCart'
import ShoppingCart from '../../components/ShoppingCart/ShoppingCart'
import {Read} from '../../actions/api'

import '../../css/pricing.css'
import '../../css/base.css'




//TODO: llevar diferentes elementos de la web a componenes
class ProductPageBase extends Component {
	
	constructor(props)
	{
		super(props)
		const{ dispatch } = this.props;
		dispatch(Read('product')())

	}
	render()
	{
		const {children, products} = this.props;	
		return (
		 <div>
		        <ShoppingCart/>
		    	<Slider/>

		        <div className="l-content">
		      	 
		        <Grid models={products} ModelComponent={ProductWithShoppingCart}/>

		        <div className="information">
		            <div className="pure-u-1-4 pure-u-md-1-2 pure-u-sm-1 ">
		                <div className="l-box">
		                    <h3 className="information-head">Get started today</h3>
		                    <p>
		                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
		                    </p>
		                </div>
		            </div>

		            <div className="pure-u-1-4 pure-u-md-1-2 pure-u-sm-1 ">
		                <div className="l-box">
		                    <h3 className="information-head">Pay monthly or annually</h3>
		                    <p>
		                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ullamco laboris nisi ut aliquip ex ea commodo
		                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.
		                    </p>
		                </div>
		            </div>

		            <div className="pure-u-1-4 pure-u-md-1-2 pure-u-sm-1">
		                <div className="l-box">
		                    <h3 className="information-head">24/7 customer support</h3>
		                    <p>
		                        Cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
		                    </p>
		                </div>
		            </div>

		            <div className="pure-u-1-4 pure-u-md-1-2 pure-u-sm-1">
		                <div className="l-box">
		                    <h3 className="information-head">Cancel your plan anytime</h3>
		                    <p>
		                        Duis aute irure dolor in reprehenderit in voluptate velit esse
		                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		                    </p>
		                </div>
		            </div>
		        </div> 
		    </div> 

		   
		 
		  {children}
		  </div>
		)
	}

}   

function mapStateToProps(state)
{
	return {
		products: state.products.items
	}
}


const WrappedProductPage = connect(mapStateToProps)(ProductPageBase)


export default WrappedProductPage