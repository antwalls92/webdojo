import React from 'react'
import SubredditList from '../../containers/Reddit/SubredditList'

const ProductAsyncPage = (dispatch) => (
	<div>
		<div className="page-header">
			<h1 className="information-head">Reddits</h1>
		<p>In this page you can select a reddit and get via a call to Reddit API the subreddits for that reddit</p>
		</div>
				
		
		<div className="l-content-centered">
			<div className="pure-g">
				<div className="pure-u-1">
						<SubredditList></SubredditList>	
				</div>
			</div>
		</div>
	</div>
)


export default ProductAsyncPage