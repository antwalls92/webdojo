import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import {Read} from '../../actions/api'

let ID = "id"
let SELECT = "select"
let ALL = "all"

class SelectBase extends Component 
{
	constructor(props){
		super(props)
		this.updateFields(props)

	}
	updateFields(props){
		
		const {field_definition, dispatch, state} = props
		let model_name = field_definition.model
		let model_lookup = field_definition.lookup
		dispatch(Read(model_name)())
		
	}

	render(){
		const{field_definition, reference, state, dispatch, useLabels} = this.props
		let model_name = field_definition.model
		let model_lookup = field_definition.lookup
		let model_lookup_description_field = field_definition.lookup_description_field
		switch(model_lookup)
		{
			case ALL:
			{
				 let items = state[model_name+'s'].items 
				 return  (
					 	<div className="pure-control-group">
							{useLabels && <label>{field_definition.label}</label>}
							<select ref={ node => reference(node) }>
							{
								Object.keys(items).map( item => 
									<option value={items[item].id}> {items[item][model_lookup_description_field]} </option>
								)
								
							}
							</select>
						</div>
					) 	
			}
		}
	}
}

const Select = connect()(SelectBase)

const RenderInput = ( field_definition , field_name, input_refs, state, useLabels, horizontal) =>
{
	switch(field_definition.type)
	{
		case ID:
			return
		case SELECT:
			return <Select field_definition = {field_definition} state={state} reference={ (node) => input_refs[field_name] = node }/> //Todo join con otras tablas
		default:
			return(
				
					!horizontal ?	<div className="pure-control-group">
							{useLabels && <label>{field_definition.label}</label>}
							<input className="pure-input-1" name={field_name} placeholder={field_definition.label} type={field_definition.type} ref={ node => input_refs[field_name] = node } />
						</div>
					:
						<input placeholder={field_definition.label} name={field_name} type={field_definition.type} ref={ node => input_refs[field_name] = node } />
				)
				
	}
}




class Form extends Component{

	constructor(props)
	{
		super(props)
	}
	
	render(){
		const {model_definition, legend, onSubmit, buttons, dispatch, state, useLabels, horizontal} = this.props
			

			var input_refs = {}
			return (<form className={ horizontal ? "pure-form pure-form-horizontal" : "pure-form pure-form-stacked" }  onSubmit={ onSubmit(input_refs) }> 
				<fieldset>
					<legend>{legend}</legend>
					{ 
						(Object.keys(model_definition)).map( input => 

							RenderInput(model_definition[input], input, input_refs, state, useLabels, horizontal)
						)
					}

					<div className="pure-controls l-box-small">
							{buttons}
					</div>
				</fieldset>
			</form>)
		}
}
	



Form.propTypes = {
	
	onSubmit: PropTypes.funct,
	legend: PropTypes.string,
	model_definition: PropTypes.object,
}

function mapStateToProps(state){
	return {state: state}
}

const WrappedForm = connect(mapStateToProps)(Form);

export default WrappedForm
