'use strict';
module.exports = function(sequelize, DataTypes) {
  var role = sequelize.define('role', {
    role: DataTypes.STRING
  });
  return role;
};