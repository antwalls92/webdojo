import React from 'react'
let AdminUserInfo = () => (
    <div className="pure-g user-info">
        <div className="pure-u-1-4">
                <img className="pure-img profile-image" src="http://www.viajes.net/fotos-comunidad/601679/comparacionesodiosasalonsodecera-13147082102020.jpg"/>
        </div>
        <div className="pure-u-3-4 l-box-small">
            <div className="pure-g">
                <div className="pure-u-1 user-name"> Antonio Paredes </div>
            </div>
            <div className="pure-g">
                <div className="pure-u-1 user-role"> Administrator </div>
            </div>
        </div>
    </div>
)

export default AdminUserInfo