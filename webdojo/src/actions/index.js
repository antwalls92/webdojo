export * from './todos'
export * from './subreddits'


export function syncError( thrown_by, error )
{
	return{
		type: "SYNC_ERROR",
		thrown_by: thrown_by,
		error: error
	}
}



