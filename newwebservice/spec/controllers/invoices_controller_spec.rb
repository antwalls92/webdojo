# spec/requests/invoices_spec.rb
require 'rails_helper'

RSpec.describe 'Invoices API', type: :request do
  # initialize test data 
  let!(:invoices) { create_list(:invoice, 10) }
  let(:invoice_id) { invoices.first.id }

  # Test suite for GET /invoices
  describe 'GET /invoices' do
    # make HTTP get request before each example
    before { get '/invoices' }

    it 'returns invoices' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /invoices/:id
  describe 'GET /invoices/:id' do
    before { get "/invoices/#{invoice_id}" }

    context 'when the record exists' do
      it 'returns the invoice' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(invoice_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:invoice_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match("{\"message\":\"Couldn't find Invoice with 'id'=100\"}")
      end
    end
  end

  # Test suite for POST /invoices
  describe 'POST /invoices' do
    # valid payload
    let(:valid_attributes) { 
      { 
          session_id: SecureRandom.uuid, 
        	amount: 180, 
        	shipping_address1: "Calle ortega nieto", 
        	billing_address1: "Avda de granada 18",
        	invoice_items: [
        		{
        			product_id: 5, 
        			amount: 50.0
        		},
        		{
        			product_id: 3, 
        			amount: 30.0
        		},
        		{
        			product_id: 3, 
        			amount: 30.0
        		},
        		{
        			product_id: 7, 
        			amount: 70.0
        		}] 
      } 
    }

    context 'when the request is valid' do
      before { post '/invoices', params: valid_attributes }

      it 'creates a invoice' do
        expect(json['amount']).to eq(180)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/invoices', params: { title: 'Foobar' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/Validation failed/)
      end
    end
  end

  # Test suite for PUT /invoices/:id
  describe 'PUT /invoices/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }

    context 'when the record exists' do
      before { put "/invoices/#{invoice_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /invoices/:id
  describe 'DELETE /invoices/:id' do
    before { delete "/invoices/#{invoice_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end