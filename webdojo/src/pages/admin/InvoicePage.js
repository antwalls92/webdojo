import React, {Component} from 'react'
import {connect} from 'react-redux'
import AdminPage from '../../components/Layouts/AdminPage'
import DeleteableList from '../../components/Common/DeleteableList'
import SimpleForm from '../../components/Common/SimpleForm'
import Invoice from '../../schema/Invoice'
import {Create, Read} from '../../actions/api'

class InvoicePage extends Component {


	componentDidMount()
	{
		const {dispatch} = this.props
		dispatch(Read('invoice')())
	}
	render(){
		const {dispatch, invoices} = this.props

		return (
		<AdminPage title="invoices">

			<div className="pure-g">
				<div className="pure-u-1 pure-controls top-buttons">
					<button className="pure-button pure-button-primary"> New </button>
				</div>
			</div>

			<div className="l-containner l-box-small">
				<SimpleForm
					model_definition={Invoice} 
					legend="Add a new invoice" 
					funtionToDispatch= { Create('invoice')} 
					buttons={<button type="submit" className="pure-button pure-button-primary">Add invoice</button>} > 
				</SimpleForm>
			</div>

			<DeleteableList models={invoices} model_name="invoice" model_description={Invoice} />
			
		</AdminPage>)
	}
}

const mapStateToProps = (state) => 
{
	return {
		invoices: state.invoices.items
	}
}

const InvoicePageWrapped = connect(mapStateToProps)(InvoicePage)


export default InvoicePageWrapped