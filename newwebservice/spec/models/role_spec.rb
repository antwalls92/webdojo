# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  role       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'


RSpec.describe Role, type: :model do
  subject { described_class.new(
    role: "admin"
    ) }
  # Association test
  
 
  # Validation test
  describe "Validations" do
  	it "is valid with valid attributes" do
  		should validate_presence_of(:role)
  	end
  	it "is invalid without role" do 
  		subject.role = nil
  		expect(subject).not_to be_valid
  	end
  	
  end
end
