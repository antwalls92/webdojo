export default {
		'id':    { value: '', type: 'id'  , label: 'Id', editable: false} ,
		'title': { value: '', type: 'text', label: 'Title', editable: true} ,
		'image': { value: '', type: 'text', label: 'Image', editable: true} ,
		'text':  { value: '', type: 'text', label: 'Text' , editable: true} ,
		'price': { value: '', type: 'number', label: 'Price' , editable: true} ,
		'offerprice': { value: '', type: 'number', label: 'Offer Price' , editable: true}
}