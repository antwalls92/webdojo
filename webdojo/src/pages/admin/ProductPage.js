import React, {Component} from 'react'
import {connect} from 'react-redux'
import AdminPage from '../../components/Layouts/AdminPage'
import DeleteableList from '../../components/Common/DeleteableList'
import SimpleForm from '../../components/Common/SimpleForm'
import ProductModel from '../../schema/Product'
import {Create, Read} from '../../actions/api'

class ProductPage extends Component {


	componentDidMount()
	{
		const {dispatch} = this.props
		dispatch(Read('product')())
	}
	render(){
		const {dispatch, products} = this.props

		return (
		<AdminPage title="Products">

			<div className="pure-g">
				<div className="pure-u-1 pure-controls top-buttons">
					<button className="pure-button pure-button-primary"> New </button>
				</div>
			</div>

			<div className="l-containner l-box-small">
				<SimpleForm
					model_definition={ProductModel} 
					legend="Add a new Product" 
					funtionToDispatch= { Create('product')} 
					buttons={<button type="submit" className="pure-button pure-button-primary">Add Product</button>} > 
				</SimpleForm>
			</div>

			<DeleteableList models={products} model_name="product" model_description={ProductModel} />
			
		</AdminPage>)
	}
}

const mapStateToProps = (state) => 
{
	return {
		products: state.products.items
	}
}

const ProductPageWrapped = connect(mapStateToProps)(ProductPage)


export default ProductPageWrapped