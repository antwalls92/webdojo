# == Schema Information
#
# Table name: invoice_items
#
#  id         :integer          not null, primary key
#  invoice_id :integer
#  product_id :integer
#  amount     :float
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class InvoiceItem < ApplicationRecord
	#association
	belongs_to :product
	belongs_to :invoice

	#validations
	validates_presence_of(:invoice, :product, :amount)
	validate :amount_of_line_should_be_equal_to_product_amount
	
	def amount_of_line_should_be_equal_to_product_amount
		if( !product.nil? && amount != product.price)
			errors.add(:amount, "Amount should be the same as the product")
		end
	end 

end
