import fetch from 'isomorphic-fetch'

export const REQUEST_POSTS = 'REQUEST_POSTS'
export const RECEIVE_POSTS = 'RECEIVE_POSTS'
export const SELECT_SUBREDDIT = 'SELECT_SUBREDIT'
export const INVALIDATE_SUBREDDIT = 'REQUEST_POSTS'

export function selectSubreddit(subreddit)
{
	return{
		type: SELECT_SUBREDDIT,
		subreddit
	}
}

export function invalidateSubreddit(subreddit)
{
	return {
		type:INVALIDATE_SUBREDDIT,
		subreddit
	}
}

function requestPosts(subreddit)
{
	return {
		type:REQUEST_POSTS,
		subreddit
	}
}

function receivePosts(subreddit, json){
	return{
		type:RECEIVE_POSTS,
		subreddit,
		posts: json.data.children.map( child => child.data),
		receivedAt: Date.now()
	}
}

function fetchPosts(subreddit){
	return dispatch => {
		dispatch(requestPosts(subreddit))
		return fetch(`https://www.reddit.com/r/${subreddit}.json`)
			.then(response => response.json(), error => console.log('An error ocurred', error))
			.then(json => dispatch(receivePosts(subreddit, json)))

	}
}

function shouldFetchPost(state, subreddit){
	const posts = state.subreddit.postsBySubreddit[subreddit]
	if(!posts){
		return true
	}else if (posts.isFetching){
		return false
	}else
	{
		return posts.didInvalidate
	}
}

export function fetchPostsIfNeeded(subreddit){
	return (dispatch, getState) => {
		if(shouldFetchPost(getState(), subreddit)){
			return dispatch(fetchPosts(subreddit))
		}
	}
}

 


