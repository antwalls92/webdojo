var db = require('./index')
var Sequelize = require('sequelize')

const Role = db.define('role', {
  role: Sequelize.STRING  
});

module.exports = Role;