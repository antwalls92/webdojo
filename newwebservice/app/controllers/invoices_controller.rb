class InvoicesController < ApplicationController
	before_action :set_invoice, only: [:show, :update, :destroy]

  # GET /invoices
  def index
    @invoices = Invoice.all
    json_response(@invoices)
  end

  # POST /invoices
  def create
    @invoice = Invoice.create!(invoice_params)
    json_response(@invoice, :created)
  end

  # GET /invoices/:id
  def show
    json_response(@invoice)
  end

  # PUT /invoices/:id
  def update
    @invoice.update(invoice_params)
    head :no_content
  end

  # DELETE /invoices/:id
  def destroy
    @invoice.destroy
    head :no_content
  end

  private

  def invoice_params
    # whitelist params
    params.permit( 
      :amount,
      :session_id, 
      :shipping_address1 , 
      :shipping_address2 , 
      :shipping_city, 
      :shipping_zip, 
      :shipping_country ,
      :billing_address1,
      :billing_address2,
      :billing_city,
      :billing_zip,
      :billing_country)
  end

  def set_invoice
    @invoice = Invoice.find(params[:id])
  end
end

