export default {
		'address1': { value: '', type: 'text', label: 'Adress1', editable: true} ,
		'address2': { value: '', type: 'text', label: 'Adress2', editable: true} ,
		'city':  { value: '', type: 'text', label: 'City' , editable: true} ,
		'zip': { value: '', type: 'text', label: 'ZIP Code' , editable: true} ,
		'country': { value: '', type: 'text', label: 'Country' , editable: true}
}