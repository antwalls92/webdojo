'use strict';
module.exports = function(sequelize, DataTypes) {
  var personal_info = sequelize.define('personal_info', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: DataTypes.STRING,
    phone:  DataTypes.NUMBER
  });

  return personal_info;
};