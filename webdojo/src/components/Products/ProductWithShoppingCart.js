import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {AddProduct} from '../../actions/shoppingcart'
import Product from './Product'


//esto es un containner!!
const ProductWithShoppingCart = ({model, dispatch}) => (
	
		<Product model={model}>
			<button className="pure-button pure-button-primary wide-button" key={model.id} onClick={(e) => { dispatch(AddProduct(model.id))}}>
				Add to the cart
			</button>
		</Product>
		
)

var wrappedProductWithShoppingCart = connect()(ProductWithShoppingCart)

export default wrappedProductWithShoppingCart