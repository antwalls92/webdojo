import { combineReducers } from 'redux'

import users from './users'
import roles from './roles'
import products from './products'
import shoppingcart from './shoppingcart'
import checkout from './checkout'
import invoices from './invoices'

const todoApp = combineReducers({
	users,
	roles,
	products,
	shoppingcart,
	checkout,
	invoices
})

export default todoApp