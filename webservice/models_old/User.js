var db = require('./index')
var Sequelize = require('sequelize');
var Role = require('./Role')

const User = db.define('user', {
  username: Sequelize.STRING,
  password: Sequelize.STRING,
  image: Sequelize.STRING,
});

User.belongsTo(Role)

module.exports = User;
